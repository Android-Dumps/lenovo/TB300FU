#!/system/bin/sh

setprop sys.lenovo.lenovolog "start"

LOGDISK="/data/local/log"

if [ -d "/data/media/0/log_cfg" ]; then
	cd /data/media/0 && chmod -R 777 log_cfg
fi

CURRENT_OFFLINE_LOG_DIR_LINK=/data/local/log/curlog
# setup log service. For eng variant, we set all log on.
if [ $(getprop persist.sys.lenovo.log) = TRUE ]; then

	FILENAME=$(date +%Y_%m_%d_%H_%M_%S)

	if [ ! -d $LOGDISK ];then
		mkdir $LOGDISK
	fi
	cd $LOGDISK

	if [ ! -d $LOGDISK"/log" ]; then
            mkdir log && chmod 777 log
	fi

	if [ ! -d "/data/media/0/log" ];then
		mkdir "/data/media/0/log"
	fi

	#Tony sun, we need remove follow cmd "chmod 777 log" at last.
	chmod 777 log

	LOGSIZE=`du -shm $LOGDISK"/log" | awk '{print $1}'`       
	setprop persist.sys.lenovo.log.size $LOGSIZE
	#if log size big than limit, permit logging
	if [ $LOGSIZE -ge $(getprop persist.sys.lenovo.log.limit) ]; then	    
                 #echo $LOGSIZE $(getprop persist.sys.lenovo.log.limit)	
                 /system/bin/cmd activity broadcast -a android.lenovo.action.LENOVO_LOG_OVERLIMIT -p com.lenovo.loggerpannel --es log_size $LOGSIZE   #send broadcast		 
	fi

	cd $LOGDISK"/log"
	if [ -e ./aplog.txt ]; then
		LASTLOGDIR=`busyox cat aplog.txt`
		chown -R media_rw:media_rw $LASTLOGDIR
	fi
	mkdir $FILENAME && chmod 777 $FILENAME
	echo $FILENAME > ./aplog.txt
	chown media_rw:media_rw aplog.txt
	chmod 777 aplog.txt
	rm $CURRENT_OFFLINE_LOG_DIR_LINK
	ln -s $LOGDISK"/log/"$FILENAME $CURRENT_OFFLINE_LOG_DIR_LINK
	chown -h shell:shell $CURRENT_OFFLINE_LOG_DIR_LINK

        setprop persist.sys.lenovo.log.path $LOGDISK"/log/"$FILENAME
        setprop persist.sys.lenovo.log.folder $FILENAME
	echo "persist.sys.lenovo.log: TRUE" >> $LOGDISK"/log/"$FILENAME/lenovolog_property
	echo "persist.sys.lenovo.log.system: $(getprop persist.sys.lenovo.log.system)" >> $LOGDISK"/log/"$FILENAME/lenovolog_property
        if [ $(getprop persist.sys.lenovo.log.system) = TRUE ]; then
            setprop ctl.start mainlog
            setprop ctl.start eventslog
        else
            setprop ctl.stop mainlog
            setprop ctl.stop eventslog
        fi
	
	echo "persist.sys.lenovo.log.kernel: $(getprop persist.sys.lenovo.log.kernel)" >> $LOGDISK"/log/"$FILENAME/lenovolog_property
        if [ $(getprop persist.sys.lenovo.log.kernel) = TRUE ]; then
            setprop ctl.start kernellog
        else
            setprop ctl.stop kernellog
        fi

	echo "persist.sys.lenovo.log.other: $(getprop persist.sys.lenovo.log.other)" >> $LOGDISK"/log/"$FILENAME/lenovolog_property
        if [ $(getprop persist.sys.lenovo.log.other) = TRUE ]; then
            setprop ctl.start performance
            setprop ctl.start tcplog
            setprop ctl.start radiolog
            setprop ctl.start tzdbg
            setprop ctl.start tadbg
        else
            setprop ctl.stop performance
            setprop ctl.stop tcplog
            setprop ctl.stop radiolog
            setprop ctl.stop tzdbg 
            setprop ctl.stop tadbg
        fi  
        if [ $(getprop persist.sys.lenovo.log.bt) = TRUE ]; then
            #BTDIR="btsnoop"
            #BTPATH=$LOGDISK"/log/"$FILENAME"/"$BTDIR
            #cd $LOGDISK"/log/"$FILENAME
            #mkdir $BTDIR && chmod 777 $BTDIR
            #setprop persist.bluetooth.btsnooppath $BTPATH
            setprop persist.bluetooth.btsnoopenable true
            setprop persist.bluetooth.btsnooplogmode full
        else
            setprop persist.bluetooth.btsnoopenable false
        fi

	echo "persist.sys.lenovo.log.qxdm: $(getprop persist.sys.lenovo.log.qxdm)" >> $LOGDISK"/log/"$FILENAME/lenovolog_property
        if [ $(getprop persist.sys.lenovo.log.qxdm) = TRUE ]; then
            setprop sys.diag.config diag_en
            sleep 3
            setprop ctl.start qxdmlog
            setprop ctl.start cnss_diag
        else
            setprop ctl.stop qxdmlog
        fi 
else
    setprop ctl.stop kernellog
    setprop ctl.stop mainlog
    setprop ctl.stop eventslog
    setprop ctl.stop performance
    setprop ctl.stop tcplog
    setprop ctl.stop radiolog
    setprop ctl.stop tzdbg
    setprop ctl.stop tadbg
    setprop ctl.stop qxdmlog
    setprop ctl.stop cnss_diag
    echo "persist.sys.lenovo.log: FALSE" >> $(getprop persist.sys.lenovo.log.path)/lenovolog_property
    setprop persist.bluetooth.btsnoopenable false
    chown -R media_rw:media_rw $(getprop persist.sys.lenovo.log.path)
    setprop persist.sys.lenovo.log.path INVALID
fi

setprop sys.lenovo.lenovolog "finish"
#setprop ctl.start mrdlog


