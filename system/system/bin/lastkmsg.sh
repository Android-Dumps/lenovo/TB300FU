#!/system/bin/sh

umask 022

if [ $(getprop persist.sys.lenovo.log.path) = INVALID ]; then
        exit
fi

if [ $(getprop persist.sys.lenovo.log) = TRUE ]; then
	LOGDIR=$(getprop persist.sys.lenovo.log.path)
else  
        if [ $(getprop persist.sys.lenovo.shutdown) = TRUE ]; then
             setprop persist.sys.lenovo.shutdown FALSE
             exit  #do not save last kmsg, if the phone is normal shut down
        fi
fi

LASTKMSG_LOGFILE=$LOGDIR"/lastkmsg"   #yexh1 



#save last kmesg
if [ -e /sys/fs/pstore/dmesg-ramoops-0 ]; then

     echo "the lastkmsg is saved at time: " > $LASTKMSG_LOGFILE
     date  >> $LASTKMSG_LOGFILE
    cat /sys/fs/pstore/dmesg-ramoops-0 >> $LASTKMSG_LOGFILE
fi
