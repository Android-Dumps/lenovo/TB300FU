#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TB300FU.mk

COMMON_LUNCH_CHOICES := \
    omni_TB300FU-user \
    omni_TB300FU-userdebug \
    omni_TB300FU-eng
