#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TB300FU.mk

COMMON_LUNCH_CHOICES := \
    lineage_TB300FU-user \
    lineage_TB300FU-userdebug \
    lineage_TB300FU-eng
